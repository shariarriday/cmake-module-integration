import QtQuick 2.15
import CircleModule 1.0

Item {

    CircleRadius {
        id: test
    }

    Rectangle {
        height: 100
        width: 100
        color: "blue"

        Text {
            anchors.centerIn: parent
            text: test.circleRadius
        }
    }
}

#ifndef CIRCLE_H
#define CIRCLE_H

#include "library_global.h"
#include <QObject>
#include <QtQml/qqmlregistration.h>

class LIBRARY_EXPORT Circle : public QObject
{
    Q_OBJECT

    QML_NAMED_ELEMENT(CircleRadius)

    Q_PROPERTY(int circleRadius READ getCircleRadius WRITE setCircleRadius NOTIFY circleRadiusChanged)

public:
    Circle(QObject *parent = nullptr);
    int getCircleRadius() const;
    void setCircleRadius(int circleRadius);

signals:
    void circleRadiusChanged(int newRadius);

private:
    int m_radius;
};

#endif // CIRCLE_H

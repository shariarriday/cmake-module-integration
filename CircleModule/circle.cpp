#include "circle.h"

Circle::Circle(QObject *parent)
    : m_radius(10)
{
}


void Circle::setCircleRadius(int circleRadius)
{
    if(m_radius == circleRadius)
        return;

    m_radius = circleRadius;
    emit circleRadiusChanged(m_radius);
}

int Circle::getCircleRadius() const
{
    return m_radius;
}

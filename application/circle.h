#ifndef CIRCLE_H
#define CIRCLE_H

#include <QObject>
#include <qqml.h>
#include <QObject>

class Circle: public QObject
{
    Q_OBJECT
    QML_NAMED_ELEMENT(Circle)

    Q_PROPERTY(int circleRadius READ getCircleRadius WRITE setCircleRadius NOTIFY circleRadiusChanged)

public:
    Circle(QObject *parent = nullptr);
    int getCircleRadius() const;
    void setCircleRadius(int circleRadius);

signals:
    void circleRadiusChanged(int newRadius);

private:
    int m_radius;
};

#endif // CIRCLE_H

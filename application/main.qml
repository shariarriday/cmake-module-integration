import QtQuick 2.15
import QtQuick.Window 2.15
import CircleModule 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    CircleRadius {
        id: testElement
        circleRadius: 16
    }

    Circle {
    }

    Text {
        id: text
        text: testElement.circleRadius
    }

}
